plasma_install_package(breath org.manjaro.breath.desktop look-and-feel lookandfeel)
plasma_install_package(breath-dark org.manjaro.breath-dark.desktop look-and-feel lookandfeel)
plasma_install_package(breath-light org.manjaro.breath-light.desktop look-and-feel lookandfeel)
